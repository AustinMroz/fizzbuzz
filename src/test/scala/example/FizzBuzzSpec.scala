package example

import org.scalatest._
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class HelloSpec extends FlatSpec with Matchers {
  val oldSOut = System.out
  val testStream = new ByteArrayOutputStream();
  Console.setOut(new PrintStream(testStream))
  FizzBuzz.main(new Array[String](0))
  val output = testStream.toString().split("\n")
  oldSOut.println(output)
  "FizzBuzz" should "print 50 lines by default" in {
    output.length shouldEqual 50
  }
  it should "say fizz on 3" in {
    output(2) shouldEqual "fizz"
  }
  it should "say buzz on 5" in {
    output(4) shouldEqual "buzz"
  }
  it should "say bang on 7" in {
    output(6) shouldEqual "bang"
  }
  Console.setOut(oldSOut)
}
