package example

import scala.collection.mutable.ArrayBuffer;

object FizzBuzz {
  def main(args: Array[String]) {
    val length = if(args.length>0)
      Integer.parseInt(args(0))
    else
      50
    val special = if(args.length>1) {
      if(args.length%2!=1) {
        println("Special numbers must be defined in pairs")
        return
      } else {
        var i = 1
        val sargs = new Array[(Int,String)]((args.length-1)/2)
        while(i<args.length) {
          sargs(i/2)=(Integer.parseInt(args(i)),args(i+1))
          i+=2
        }
        sargs
      }
    } else {
      Array((3,"fizz"),(5,"buzz"),(7,"bang"))
    }
    fizzbuzz(length, special)
  }

  def fizzbuzz(count: Int, special: Array[(Int,String)]) {
    var llz = 32
    for(tuple <- special)
      if(Integer.numberOfLeadingZeros(tuple._1)<llz)
        llz = Integer.numberOfLeadingZeros(tuple._1)
    var as = 1<<(32-llz)
    val al = new Array[Int](as)
    val oa = new ArrayBuffer[String](special.length)
    as-=1
    for(i <- 0 to (special.length-1))
      al(special(i)._1) |= 1<<i;
    var i = 1
    while(i<=count) {
      while(al(i&as)==0) {
        println(i)
        i+=1
        if(i>count)
          return;
      }
      oa.clear
      for(si <- 0 to (31-Integer.numberOfLeadingZeros(al(i&as)))) {
        if((al(i&as)&(1<<si)) != 0) {
          oa += special(si)._2
          al((i+special(si)._1)&as)|=1<<si
        }
      }
      println(oa.mkString(" "))
      al(i&as)=0
      i+=1
    }
  }
}
