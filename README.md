# FizzBuzz
A classic problem used to test the bare minimum adequacy of a programmer in interviews,
The fizzbuzz problem often becomes a game of one-upmanship.

Some will do it without if statements, in unusual languages or make it a game of code golf.

This is an implementation that allows:
- Special values to be defined at run time
- non-special cases to have run time unaffected by the number of special cases
- lazy evaluation of output (increasing the maximum count output does not increase memory use)

## Usage
The program expects arguments of the following format
```
fizzbuzz [Count [Mod Title]...]
```
The project is setup to be built with sbt, so the default case could be run with
```
sbt
sbt:FizzBuzz> run 50 3 fizz 5 buzz 7 bang
```


